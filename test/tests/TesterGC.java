
package tests;

import data.Dao;
import entites.Region;
import entites.Client;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;

public class TesterGC {
      
    Dao dao;
    
    @Before
    public  void initialisationDAO() {
    
        dao=new Dao();
    }
    
    @Test
    public void testerCaclientParAnnee() {

         Client c=dao.getClientDeNumero(101L);
         
         assertEquals("Erreur Calcul CA",575.5, c.caClient(2018),0.001);
    }
    
    
    
        @Test
        public void testerCaclientParAnneeEtMois() {
    
             Client c=dao.getClientDeNumero(101L);
    
         
             assertEquals("Erreur Calcul CA",1131.28, c.caClient(2017,12),0.001);
        }
        
           @Test
        public void testerCaregionParAnneeEtMois() {
    
             Region r=dao.getRegionDeCode("GE");
    
            assertEquals("Erreur Calcul CA",1432.82, r.caRegion(2017,12),0.001);
            // 12/2017 GE = 1432.82 une seule facture réglée 
        }
           
           
         @Test
        public void testerCaregionParAnnee() {
    
             Region r=dao.getRegionDeCode("HDF");
    
            assertEquals("Erreur Calcul CA",1510.64, r.caRegion(2018),0.001);
            //2017 HDF = 567.50 + 98 + 723.45 + 563.78 = 1952.73
            //2018 HDF =  235.5 + 340 + 340 + 216.8 + 378.34 = 1510.64 
        }
}